#!/bin/bash

# Tested on 
# - Windows 7

#export PATH="$PATH:/c/cygwin/bin"
WGET="c:/cygwin/bin/wget"
UNZIP="c:/cygwin/bin/unzip"
GIT="c:/Program Files (x86)/Git/bin/git"
MINGW_DIR="c:/MinGW"

NO_DOWNLOAD=0
BUILD_MSPDEBUG=1
BUILD_MSPGCC=0
BUILD_LIBUSB=1
BUILD_REGEX=1
BUILD_READLINE=1

ACTION="$1"

case $ACTION in
	build)
	;;
	clean)
		exit 0
	;;
	*)
		echo "Unknown action"
		exit 1
	;;
esac

function printBuildUsage()
{
	echo "$0 build <version> [<toolchain-id-prefix> [<name>]]"
	exit 1
}

VERSION="$2"
TOOLCHAIN_ID_PREFIX="$3"
TOOLCHAIN_DESC="$4"

if [ "$TOOLCHAIN_ID_PREFIX" = "" ]; then
	TOOLCHAIN_ID_PREFIX="dk.xpg.toolchain"
fi
if [ "$TOOLCHAIN_DESC" = "" ]; then
	TOOLCHAIN_DESC="xpg.dk pre-build toolchain $VERSION"
fi

if [ "$VERSION" = "" -o "$TOOLCHAIN_ID_PREFIX" = "" -o "$TOOLCHAIN_DESC" = "" ]; then
	printBuildUsage
fi

ARCH="i686"
OS="windows"
TOOLCHAIN_NAME="msp430-toolchain-${OS}-${ARCH}"
SCRIPT_HOME="$PWD"

SRC_ROOT="${SCRIPT_HOME}/src"
export MSP430_ROOT=$SRC_ROOT
mkdir -p $SRC_ROOT

TARBALL_ROOT="${SCRIPT_HOME}/tars"
mkdir -p $TARBALL_ROOT

INSTALL_DIR="${SCRIPT_HOME}/install/${TOOLCHAIN_NAME}"
BUILD_ROOT="${SCRIPT_HOME}/BUILD/${TOOLCHAIN_NAME}"

# Install-directory relative to the BUILD-directory.
# This solves MinGW problems with absolute paths.
REL_INSTALL_DIR="../../../../install"
mkdir -p $BUILD_ROOT
mkdir -p $INSTALL_DIR

PACKAGE_DIR="${SCRIPT_HOME}/package/${TOOLCHAIN_NAME}-${VERSION}"

function gitUpdate()
{
	local SRC_ROOT=$1
	local DIR=$2
	local URL=$3
	if [ -e "$DIR" ]; then
		cd $DIR
		echo "Updating $DIR..."
		"$GIT" pull
	else
		cd $SRC_ROOT
		"$GIT" clone "$URL" "$DIR"
        fi
	cd $DIR
	"$GIT" log -1 | grep commit > git-version
}

function fetch()
{
	local DOWNLOAD_DIR="$1"
	local EXTRACT_DIR="$2"
	local FILENAME="$3"
	local URL="$4"
	
	if [ ! -e "$DOWNLOAD_DIR/$FILENAME" ]; then
		pushd $DOWNLOAD_DIR
		$WGET "$URL" -O "$FILENAME"
		popd
	fi
	
#	pushd $EXTRACT_DIR
#	unzip $DOWNLOAD_DIR/$FILENAME
#	popd
}

function generate_wrapper()
{
	local TEMPLATE="$1"
	local WRAPPER="$2"
	local EXEC="$3"
	cat $SCRIPT_HOME/$TEMPLATE | sed -e "s/%EXEC%/$EXEC/g" > $WRAPPER
	chmod +x $WRAPPER
}

MSPDEBUG_DIR="${SRC_ROOT}/mspdebug"

echo "Checkout root is $SRC_ROOT"

MSPGCC_ZIP="mspgcc-20120406-p20120502.zip"
MSPGCC_SOURCE="http://sourceforge.net/projects/mspgcc/files/Windows/mingw32/mspgcc-20120406-p20120502.zip"

MINGW_REGEX_ZIP="regex-2.7-bin.zip"
MINGW_REGEX_SOURCE="http://sourceforge.net/projects/gnuwin32/files/regex/2.7/regex-2.7-bin.zip/download"

MINGW_USB_ZIP="libusb-win32-bin-1.2.5.0.zip"
MINGW_USB_SOURCE="http://sourceforge.net/projects/libusb-win32/files/libusb-win32-releases/1.2.5.0/libusb-win32-bin-1.2.5.0.zip"

MINGW_READLINE_ZIP="readline-5.0-1-bin.zip"
MINGW_READLINE_SOURCE="http://sourceforge.net/projects/gnuwin32/files/readline/5.0-1/readline-5.0-1-bin.zip"
if [ "$NO_DOWNLOAD" -ne "1" ]; then
	fetch "$TARBALL_ROOT" "$SRC_ROOT" "mspgcc-20120406-p20120502.zip" "$MSPGCC_SOURCE"
	fetch "$TARBALL_ROOT" "$SRC_ROOT" "$MINGW_REGEX_ZIP" "$MINGW_REGEX_SOURCE"
	fetch "$TARBALL_ROOT" "$SRC_ROOT" "$MINGW_USB_ZIP" "$MINGW_USB_SOURCE"
	fetch "$TARBALL_ROOT" "$SRC_ROOT" "$MINGW_READLINE_ZIP" "$MINGW_READLINE_SOURCE"
	gitUpdate "$SRC_ROOT" "$MSPDEBUG_DIR" "git://mspdebug.git.sourceforge.net/gitroot/mspdebug/mspdebug"
	
	mkdir -p $INSTALL_DIR
fi

if [ "$BUILD_MSPGCC" -eq "1" ]; then
	cd "$INSTALL_DIR"
	$UNZIP -n "$TARBALL_ROOT/$MSPGCC_ZIP"
fi

if [ "$BUILD_LIBUSB" -eq "1" ]; then
	cd "$SRC_ROOT"
	$UNZIP -n "$TARBALL_ROOT/$MINGW_USB_ZIP"
	cp "libusb-win32-bin-1.2.5.0/include/lusb0_usb.h" "c:/mingw/include/usb.h"
	cp "libusb-win32-bin-1.2.5.0/lib/gcc/libusb.a" "c:/mingw/lib"
	cp libusb-win32-bin-1.2.5.0/bin/x86/libusb0_x86.dll $INSTALL_DIR/bin/libusb0.dll
fi

if [ "$BUILD_REGEX" -eq "1" ]; then
	mkdir "$SRC_ROOT/regex"
	cd "$SRC_ROOT/regex"
	$UNZIP -n "$TARBALL_ROOT/$MINGW_REGEX_ZIP"
	cp include/* "c:/mingw/include"
	cp lib/* "c:/mingw/lib"
	cp bin/*.dll $INSTALL_DIR/bin
fi

if [ "$BUILD_READLINE" -eq "1" ]; then
	mkdir "$SRC_ROOT/readline"
	cd "$SRC_ROOT/readline"
	$UNZIP -n "$TARBALL_ROOT/$MINGW_READLINE_ZIP"
	cp -R include/* "c:/mingw/include"
	cp lib/* "c:/mingw/lib"
	cp bin/*.dll $INSTALL_DIR/bin
fi

#export PATH="${INSTALL_DIR}/bin:${PATH}"

if [ "$BUILD_MSPDEBUG" -eq "1" ]; then
	cd ${MSPDEBUG_DIR}
	/mingw/bin/mingw32-make PREFIX=${INSTALL_DIR} install
fi

exit 0

# Setup wrapper scripts
generate_wrapper "wrapper-template" "${INSTALL_DIR}/bin/msp430-gcc-wrapper" "msp430-gcc"
generate_wrapper "wrapper-template" "${INSTALL_DIR}/bin/msp430-gdb-wrapper" "msp430-gdb"
generate_wrapper "wrapper-template" "${INSTALL_DIR}/bin/mspdebug-wrapper" "mspdebug"

# Copy dependencies
#cp -a /usr/lib/libgmp.so* $INSTALL_DIR/lib
#cp -a /usr/lib/libmpc.so* $INSTALL_DIR/lib
#cp -a /usr/lib/libmpfr.so* $INSTALL_DIR/lib

# Create package
mkdir -p $PACKAGE_DIR
cp -R $INSTALL_DIR/* $PACKAGE_DIR
cat > $PACKAGE_DIR/tool.info<<EOF
id=$TOOLCHAIN_ID_PREFIX.$OS.$ARCH
name=$TOOLCHAIN_DESC
os=$OS
arch=$ARCH
toolsProvided=msp430-gdb,mspdebug,msp430-gcc,msp430-ar
mspdebug=bin/mspdebug-wrapper
msp430-gdb=bin/msp430-gdb-wrapper
msp430-gcc=bin/msp430-gcc-wrapper
msp430-ar=bin/msp430-gcc-wrapper
EOF

mkdir -p $PACKAGE_DIR/docs/binutils
cp $BINUTILS_DIR/COPYING* $PACKAGE_DIR/docs/binutils
BINUTILS_REV=$(cat $BINUTILS_DIR/git-version)
cp $MINGW_DIR/bin/libintl*.dll $PACKAGE_DIR/bin
cp $MINGW_DIR/bin/libgcc_s*.dll $PACKAGE_DIR/bin
cp $MINGW_DIR/bin/libiconv*.dll $PACKAGE_DIR/bin

mkdir -p $PACKAGE_DIR/docs/gcc
cp $GCC_DIR/COPYING* $PACKAGE_DIR/docs/gcc
GCC_REV=$(cat $GCC_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/msp430-libc
cp $MSP430_LIBC_DIR/COPYING* $PACKAGE_DIR/docs/msp430-libc
MSP430_LIBC_REV=$(cat $MSP430_LIBC_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/msp430mcu
cp $MSP430_MCU_DIR/COPYING* $PACKAGE_DIR/docs/msp430mcu
MSP430_MCU_REV=$(cat $MSP430_MCU_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/mspdebug
cp $MSPDEBUG_DIR/COPYING* $PACKAGE_DIR/docs/mspdebug
MSPDEBUG_REV=$(cat $MSPDEBUG_DIR/git-version)

mkdir -p $PACKAGE_DIR/docs/gdb
cp $MSP430_GDB_DIR/COPYING* $PACKAGE_DIR/docs/gdb

MSPGCC_REV=$(cat $MSPGCC_DIR/git-version)

cat > $PACKAGE_DIR/README<<EOF
This package is build from the following pieces of software:
MinGW http://www.mingw.org
$GDB_SOURCE
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/binutils $BINUTILS_REV
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/gcc $GCC_REV
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/msp430-libc $MSP430_LIBC_REV
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/msp430mcu $MSP430_MCU_REV
git://mspgcc.git.sourceforge.net/gitroot/mspgcc/mspgcc $MSPGCC_REV
git://mspdebug.git.sourceforge.net/gitroot/mspdebug/mspdebug $MSPDEBUG_REV
EOF
