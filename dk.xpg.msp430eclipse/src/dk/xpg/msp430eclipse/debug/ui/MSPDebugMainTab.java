package dk.xpg.msp430eclipse.debug.ui;

import org.eclipse.cdt.debug.core.ICDTLaunchConfigurationConstants;
import org.eclipse.cdt.debug.mi.core.IMILaunchConfigurationConstants;
import org.eclipse.cdt.dsf.gdb.IGDBLaunchConfigurationConstants;
import org.eclipse.cdt.launch.ui.CMainTab;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

public class MSPDebugMainTab extends CMainTab {
	public MSPDebugMainTab() {
		super(CMainTab.DONT_CHECK_PROGRAM);
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy config) {
		super.setDefaults(config);
		
		config.setAttribute(
				ICDTLaunchConfigurationConstants.ATTR_DEBUGGER_START_MODE,
				IGDBLaunchConfigurationConstants.DEBUGGER_MODE_REMOTE);
		config.setAttribute(IGDBLaunchConfigurationConstants.ATTR_HOST,
				"localhost");
		config.setAttribute(IGDBLaunchConfigurationConstants.ATTR_REMOTE_TCP, true);
		config.setAttribute(IGDBLaunchConfigurationConstants.ATTR_PORT, "2000");
		config.setAttribute(IGDBLaunchConfigurationConstants.ATTR_DEBUG_NAME,
				"${dk.xpg.msp430eclipse.variable.debugger}");
		config.setAttribute(IGDBLaunchConfigurationConstants.ATTR_DEBUGGER_UPDATE_THREADLIST_ON_SUSPEND, false);
	}
}
