package dk.xpg.msp430eclipse.managedbuild;

import org.eclipse.cdt.core.cdtvariables.CdtVariableException;
import org.eclipse.cdt.managedbuilder.core.IConfiguration;
import org.eclipse.cdt.managedbuilder.macros.BuildMacroException;
import org.eclipse.cdt.managedbuilder.macros.IBuildMacro;
import org.eclipse.cdt.managedbuilder.macros.IBuildMacroProvider;
import org.eclipse.cdt.managedbuilder.macros.IBuildMacroStatus;
import org.eclipse.cdt.managedbuilder.macros.IConfigurationBuildMacroSupplier;
import org.eclipse.core.resources.IProject;

import dk.xpg.msp430eclipse.MSP430PropertyManager;

public class ConfigurationBuildMacroSupplier implements
		IConfigurationBuildMacroSupplier {

	@Override
	public IBuildMacro getMacro(String macroName, IConfiguration configuration,
			IBuildMacroProvider provider) {
		for(ConfigurationMacroNames m: ConfigurationMacroNames.values() ) {
			if( macroName.equals(m.name())) {
				return new BuildMacro(m.name(), configuration);
			}
		}
		return null;
	}

	@Override
	public IBuildMacro[] getMacros(IConfiguration configuration,
			IBuildMacroProvider provider) {
		BuildMacro[] macros = new BuildMacro[ConfigurationMacroNames.values().length];
		int i=0;
		for(ConfigurationMacroNames m: ConfigurationMacroNames.values() ) {
			macros[i++] = new BuildMacro(m.name(), configuration); 
		}
		return macros;
	}

	
	private class BuildMacro implements IBuildMacro {
		private String name;
		private IConfiguration configuration;
		
		public BuildMacro(String name, IConfiguration configuration) {
			this.name = name;
			this.configuration = configuration;
		}
		
		@Override
		public String getName() {
			return name;
		}

		@Override
		public int getValueType() {
			return IBuildMacro.VALUE_TEXT;
		}

		@Override
		public int getMacroValueType() {
			return IBuildMacro.VALUE_TEXT;
		}

		@Override
		public String getStringValue() throws BuildMacroException {
			MSP430PropertyManager propManager = MSP430PropertyManager.getInstance((IProject)configuration.getOwner());
			return propManager.getPropertyValue(name);
		}

		@Override
		public String[] getStringListValue() throws BuildMacroException {
			throw new BuildMacroException(new CdtVariableException(
					IBuildMacroStatus.TYPE_MACRO_NOT_STRINGLIST, getName(),
					null, null));
		}

	}
}
