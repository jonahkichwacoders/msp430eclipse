package dk.xpg.msp430eclipse.managedbuild;

import org.eclipse.cdt.managedbuilder.core.IManagedCommandLineGenerator;
import org.eclipse.cdt.managedbuilder.core.IManagedCommandLineInfo;
import org.eclipse.cdt.managedbuilder.core.ITool;
import org.eclipse.cdt.managedbuilder.internal.core.ManagedCommandLineGenerator;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

@SuppressWarnings("restriction")
public class MSP430GCCCommandLineGenerator implements IManagedCommandLineGenerator {

	public MSP430GCCCommandLineGenerator() {
	}

	public IManagedCommandLineInfo generateCommandLineInfo(ITool tool,
			String commandName, String[] flags, String outputFlag,
			String outputPrefix, String outputName, String[] inputResources,
			String commandLinePattern) {

		try {
			ToolType type = ToolType.getTypeFromCommandName(commandName);

			if (type != null) {
				commandName = MSP430Activator.getDefault().getTool(type);
			}
		} catch (ToolchainNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ManagedCommandLineGenerator.getCommandLineGenerator().generateCommandLineInfo(tool, commandName, flags,
				outputFlag, outputPrefix, outputName, inputResources,
				commandLinePattern);
	}
}
