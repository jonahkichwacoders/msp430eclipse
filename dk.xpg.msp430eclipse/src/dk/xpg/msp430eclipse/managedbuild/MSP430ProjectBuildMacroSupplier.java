package dk.xpg.msp430eclipse.managedbuild;

import org.eclipse.cdt.core.cdtvariables.CdtVariableException;
import org.eclipse.cdt.managedbuilder.core.IManagedProject;
import org.eclipse.cdt.managedbuilder.macros.BuildMacroException;
import org.eclipse.cdt.managedbuilder.macros.IBuildMacro;
import org.eclipse.cdt.managedbuilder.macros.IBuildMacroProvider;
import org.eclipse.cdt.managedbuilder.macros.IBuildMacroStatus;
import org.eclipse.cdt.managedbuilder.macros.IProjectBuildMacroSupplier;
import org.eclipse.core.resources.IProject;

import dk.xpg.msp430eclipse.MSP430PropertyManager;

public class MSP430ProjectBuildMacroSupplier implements
		IProjectBuildMacroSupplier {

	@Override
	public IBuildMacro getMacro(String macroName, IManagedProject project,
			IBuildMacroProvider provider) {
		for(MSP430ProjectMacroNames m: MSP430ProjectMacroNames.values() ) {
			if( macroName.equals(m.name())) {
				return new BuildMacro(m, project);
			}
		}
		return null;
	}

	@Override
	public IBuildMacro[] getMacros(IManagedProject project,
			IBuildMacroProvider provider) {
		BuildMacro[] macros = new BuildMacro[MSP430ProjectMacroNames.values().length];
		int i=0;
		for(MSP430ProjectMacroNames m: MSP430ProjectMacroNames.values() ) {
			macros[i++] = new BuildMacro(m, project); 
		}
		return macros;
	}

	private class BuildMacro implements IBuildMacro {
		private MSP430ProjectMacroNames name;
		private IManagedProject project;
		
		public BuildMacro(MSP430ProjectMacroNames name, IManagedProject project) {
			this.name = name;
			this.project = project;
		}
		
		@Override
		public String getName() {
			return name.getName();
		}

		@Override
		public int getValueType() {
			return IBuildMacro.VALUE_TEXT;
		}

		@Override
		public int getMacroValueType() {
			return IBuildMacro.VALUE_TEXT;
		}

		@Override
		public String getStringValue() throws BuildMacroException {
			return name.resolve((IProject)project.getOwner());
			/*MSP430PropertyManager propManager = MSP430PropertyManager.getInstance((IProject)project.getOwner());
			return propManager.getPropertyValue(name);*/
		}

		@Override
		public String[] getStringListValue() throws BuildMacroException {
			throw new BuildMacroException(new CdtVariableException(
					IBuildMacroStatus.TYPE_MACRO_NOT_STRINGLIST, getName(),
					null, null));
		}

	}

}
