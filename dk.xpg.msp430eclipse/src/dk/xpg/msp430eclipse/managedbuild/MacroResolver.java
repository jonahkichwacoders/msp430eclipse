package dk.xpg.msp430eclipse.managedbuild;

import org.eclipse.core.resources.IProject;

interface MacroResolver {
	String resolveMacro(IProject project, String name);
}
