package dk.xpg.msp430eclipse.managedbuild;

import org.eclipse.core.resources.IProject;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.ToolManager;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class ToolchainResolver implements MacroResolver {

	private ToolType toolType;

	public ToolchainResolver(ToolType toolType) {
		this.toolType = toolType;
	}
	
	@Override
	public String resolveMacro(IProject project, String name) {
		ToolManager m = MSP430Activator.getDefault().getToolchainManager();
		try {
			return m.getCurrentProvider(toolType).getExecutable(toolType);
		} catch (ToolchainNotFoundException e) {
			return "";
		}
	}

}
