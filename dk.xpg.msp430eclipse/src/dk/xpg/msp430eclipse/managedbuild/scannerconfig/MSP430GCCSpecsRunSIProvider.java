package dk.xpg.msp430eclipse.managedbuild.scannerconfig;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import org.eclipse.cdt.make.core.MakeCorePlugin;
import org.eclipse.cdt.make.internal.core.scannerconfig2.GCCSpecsRunSIProvider;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.MSP430PropertyManager;
import dk.xpg.msp430eclipse.preferences.PreferenceConstants;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;

@SuppressWarnings("restriction")
public class MSP430GCCSpecsRunSIProvider extends GCCSpecsRunSIProvider {

	@Override
	protected boolean initialize() {
		boolean superOk = super.initialize();

		if (superOk) {
			String targetMcu = MSP430PropertyManager.getInstance(
					resource.getProject()).getPropertyValue("MSP430TARGETMCU");
			for (int i = 0; i < fCompileArguments.length; ++i) {
				fCompileArguments[i] = fCompileArguments[i].replaceAll(
						"\\$\\{MSP430TARGETMCU\\}", targetMcu);
			}

			try {
				ToolType type = ToolType.getTypeFromCommandName(fCompileCommand.toFile().getName());
				
				if( type != null) {
					fCompileCommand = new Path(MSP430Activator.getDefault().getTool(type));
				}
			} catch (ToolchainNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return superOk;
	}

}
