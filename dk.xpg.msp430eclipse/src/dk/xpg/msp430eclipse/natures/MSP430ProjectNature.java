package dk.xpg.msp430eclipse.natures;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;

public class MSP430ProjectNature implements IProjectNature {
	private IProject project;

	@Override
	public void configure() throws CoreException {
	}

	@Override
	public void deconfigure() throws CoreException {
	}

	@Override
	public IProject getProject() {
		return project;
	}

	@Override
	public void setProject(IProject project) {
		this.project = project;
	}

	public static void addMSP430Nature(IProject project) throws CoreException {
		/* Borrowed from AVR Eclipse plugin */
		final String natureid = "dk.xpg.msp430eclipse.msp430nature";
		IProjectDescription description = project.getDescription();
		String[] oldnatures = description.getNatureIds();

		// Check if the project already has an MSP430 nature
		for (int i = 0; i < oldnatures.length; i++) {
			if (natureid.equals(oldnatures[i]))
				return; // return if MSP430 nature already set
		}
		String[] newnatures = new String[oldnatures.length + 1];
		System.arraycopy(oldnatures, 0, newnatures, 0, oldnatures.length);
		newnatures[oldnatures.length] = natureid;
		description.setNatureIds(newnatures);
		project.setDescription(description, new NullProgressMonitor());
	}

	public static boolean hasMSP430Nature(IProject project) throws CoreException {
		final String natureid = "dk.xpg.msp430eclipse.msp430nature";
		IProjectDescription description = project.getDescription();
		String[] natures = description.getNatureIds();
		for (int i = 0; i < natures.length; i++) {
			if (natureid.equals(natures[i]))
				return true;
		}
		return false;
	}
}
