package dk.xpg.msp430eclipse.preferences;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.SystemToolProvider;
import dk.xpg.msp430eclipse.toolinfo.Targets;

public class MSP430PreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	private Map<ToolType, ComboFieldEditor> toolProviderSelectors;
	private Map<ToolType, DirectoryFieldEditor> toolPrefixSelectors;

	public MSP430PreferencePage() {
		super(GRID);
		setPreferenceStore(MSP430Activator.getDefault().getPreferenceStore());
		setDescription("MSP430 Preferences");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	public void createFieldEditors() {

		toolProviderSelectors = new HashMap<ToolType, ComboFieldEditor>();
		toolPrefixSelectors = new HashMap<ToolType, DirectoryFieldEditor>();

		IPreferenceStore prefs = MSP430Activator.getDefault()
				.getPreferenceStore();

		for (ToolType type : ToolType.values()) {
			List<IMSP430ToolProvider> providers = MSP430Activator.getDefault()
					.getToolchainManager().getProviders(type);
			String[][] values = new String[providers.size()][2];

			for (int i = 0; i < providers.size(); i++) {
				IMSP430ToolProvider c = providers.get(i);
				values[i][0] = c.getName();
				values[i][1] = c.getProviderId();
			}
			ComboFieldEditor comboEditor = new ComboFieldEditor(
					type.getProviderPreferenceName(), type.getName()
							+ " Provider:", values, getFieldEditorParent());
			DirectoryFieldEditor directoryEditor = new DirectoryFieldEditor(
					SystemToolProvider.getPrefixPreferenceName(type),
					type.getName() + " binary location", getFieldEditorParent());

			toolProviderSelectors.put(type, comboEditor);
			toolPrefixSelectors.put(type, directoryEditor);

			addField(comboEditor);
			addField(directoryEditor);

			if (prefs.getString(type.getProviderPreferenceName()).equals(
					SystemToolProvider.class.getCanonicalName())) {
				directoryEditor.setEnabled(true, getFieldEditorParent());
			} else {
				directoryEditor.setEnabled(false, getFieldEditorParent());
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		super.propertyChange(event);

		if (event.getProperty().equals(FieldEditor.VALUE)) {
			for (ToolType type : ToolType.values()) {
				if (event.getSource() == toolProviderSelectors.get(type)) {

					if (event.getNewValue().equals(
							SystemToolProvider.class.getCanonicalName())) {
						toolPrefixSelectors.get(type).setEnabled(true,
								getFieldEditorParent());
					} else {
						toolPrefixSelectors.get(type).setEnabled(false,
								getFieldEditorParent());
					}

				}
			}
		}
	}
	
	@Override
	protected void performApply() {
		super.performApply();
		Targets.getInstance().clearList();
	}
	
	@Override
	public boolean performOk() {
		Targets.getInstance().clearList();
		return super.performOk();		
	}
}