package dk.xpg.msp430eclipse.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_MSPDEBUG_PATH = "MSPDebugPath";

	public static final String P_USE_BUNDLED_MSPDEBUG = "useBundledMSPDebug";

	public static final String P_GCC_PREFIX = "GCCPrefix";
	public static final String P_USE_BUNDLED_GCC = "UseBundledGCC";
	
	public static final String P_DEBUGGER = "Debugger";
	public static final String P_COMPILER = "Compiler";
	public static final String P_COMPILER_SYSTEM_LOCATION = "CompilerPrefix";
	public static final String P_DEBUGGER_SYSTEM_LOCATION = "DebuggerPrefix";
	public static final String P_TOOL_DIRECTORIES = "ToolDirectories";

	public static final String P_TOOLS_PROVIDED = "toolsProvided";
	
	public static final String P_DEVICE_MAP = "deviceMap";
}
