package dk.xpg.msp430eclipse.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.SystemToolProvider;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = MSP430Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.P_TOOL_DIRECTORIES, "");
		store.setDefault(PreferenceConstants.P_TOOLS_PROVIDED, "");
		store.setDefault(PreferenceConstants.P_DEVICE_MAP, "");

		for(ToolType t: ToolType.values()) {
			store.setDefault(t.getProviderPreferenceName(), SystemToolProvider.class.getCanonicalName());
			store.setDefault(SystemToolProvider.getPrefixPreferenceName(t), "");
		}
	}

}
