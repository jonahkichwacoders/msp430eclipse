package dk.xpg.msp430eclipse.toolchain;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import dk.xpg.msp430eclipse.preferences.PreferenceConstants;

public class GenericToolProvider implements IMSP430ToolProvider {

	private static final String osName = System.getProperty("os.name")
			.toLowerCase();
	private static final String osArch = System.getProperty("os.arch")
			.toLowerCase();

	private String name;
	private String id;
	private String path;
	private Map<ToolType, String> tools;

	public GenericToolProvider(String path) throws Exception {
		File infoFile = new File(path, "tool.info");
		InputStream inStream = new FileInputStream(infoFile);
		Properties props = new Properties();
		props.load(inStream);
		inStream.close();

		if (!(osName.startsWith("windows") && props.getProperty("os", "")
				.startsWith("windows"))) {
			if (!props.getProperty("os", "").equals(osName)) {
				throw new Exception("Os invalid");
			}
			if (!props.getProperty("arch", "").equals(osArch)) {
				throw new Exception("Arch invalid");
			}
		}

		String providedTools = props.getProperty(PreferenceConstants.P_TOOLS_PROVIDED, "");
		name = props.getProperty("name", path);
		id = props.getProperty("id");

		tools = new HashMap<ToolType, String>();
		this.path = path;

		StringTokenizer tok = new StringTokenizer(providedTools, ",");
		while (tok.hasMoreTokens()) {
			String t = tok.nextToken();
			ToolType toolType = ToolType.getTypeFromCommandName(t);
			if (toolType != null) {
				tools.put(toolType, props.getProperty(t));
			}
		}
	}

	public String getProvidedTools() {
		StringBuilder builder = new StringBuilder();

		for (ToolType t : getTypes()) {
			builder.append(t.getName());
			builder.append(",");
		}
		return builder.toString();
	}

	@Override
	public String getExecutable(ToolType type) {
		return new File(path, tools.get(type)).getAbsolutePath();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Set<ToolType> getTypes() {
		return tools.keySet();
	}

	@Override
	public String getProviderId() {
		return getClass().getCanonicalName() + "-" + id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IMSP430ToolProvider) {
			return getProviderId().equals( ((IMSP430ToolProvider)obj).getProviderId());
		}
		return false;
	}
}
