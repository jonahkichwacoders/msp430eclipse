package dk.xpg.msp430eclipse.toolchain;

import java.util.Set;

public interface IMSP430ToolProvider {
	public String getExecutable(ToolType type);;
	public String getName();
	public Set<ToolType> getTypes();
	public String getProviderId();
	
	public enum ToolType {
		GCC("GCC", "msp430-gcc"),
		GDB("GDB", "msp430-gdb"),
		AR("AR", "msp430-ar"),
		MSPDEBUG("MSPDEBUG", "mspdebug"),
		MAKE("MAKE", "make");
		
		private String name;
		private String commandName;
		
		public String getName() {
			return name;
		}
		
		public String getCommandName() {
			return commandName;
		}
		
		public String getProviderPreferenceName() {
			return "TOOL_PROVIDER_" + getName();
		}
		
		private ToolType(String name, String commandName) {
			this.name = name;
			this.commandName = commandName;
		}
		
		static public ToolType getTypeFromCommandName(String commandName) {
			for(ToolType type: ToolType.values()) {
				if( type.getCommandName().equals(commandName) ) {
					return type;
				}
			}
			return null;
		}
	}
}
