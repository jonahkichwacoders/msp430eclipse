package dk.xpg.msp430eclipse.toolchain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.eclipse.jface.preference.IPreferenceStore;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.preferences.PreferenceConstants;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;

public class ToolManager {
	private Map<ToolType, List<IMSP430ToolProvider>> tools; 
	private MSP430Activator plugin;
	
	public ToolManager(MSP430Activator plugin) {
		tools = new HashMap<ToolType, List<IMSP430ToolProvider>>();

		for (ToolType type : ToolType.values()) {
			tools.put(type, new ArrayList<IMSP430ToolProvider>());
		}

		registerToolProvider(new SystemToolProvider());
		
		this.plugin = plugin;
			
	}

	public void registerToolProvider(IMSP430ToolProvider provider) {
		Set<ToolType> types = provider.getTypes();

		for (ToolType t : types) {
			tools.get(t).add(provider);
		}
	}

	public void removeToolProvider(IMSP430ToolProvider provider) {
		Set<ToolType> types = provider.getTypes();
		for (ToolType t : types) {
			tools.get(t).remove(provider);
		}
	}
	
	public List<IMSP430ToolProvider> getProviders(ToolType type) {
		return Collections.unmodifiableList(tools.get(type));
	}
	
	public IMSP430ToolProvider getCurrentProvider(ToolType type) throws ToolchainNotFoundException {
		IPreferenceStore prefs = MSP430Activator.getDefault().getPreferenceStore();
		return getProvider(type, prefs.getString(type.getProviderPreferenceName()));
	}

	public IMSP430ToolProvider getProvider(ToolType type,
			String providerId) throws ToolchainNotFoundException {
		for (IMSP430ToolProvider p : tools.get(type)) {
			if (p.getProviderId().equals(providerId)) {
				return p;
			}
		}
		throw new ToolchainNotFoundException(type, providerId);
	}

}
