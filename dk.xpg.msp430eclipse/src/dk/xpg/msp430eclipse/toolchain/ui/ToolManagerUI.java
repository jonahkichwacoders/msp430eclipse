package dk.xpg.msp430eclipse.toolchain.ui;

import java.util.StringTokenizer;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.preferences.PreferenceConstants;
import dk.xpg.msp430eclipse.toolchain.GenericToolProvider;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;

public class ToolManagerUI extends Dialog {

	private Table toolsTable;
	private Button activateButton;
	private Button addButton;
	private Button removeButton;
	private Button closeButton;
	private IPreferenceStore prefStore;
	private Shell dialogShell;
		
	public ToolManagerUI(Shell parent, int style) {
		super(parent, style);
		
	}
	
	public ToolManagerUI(Shell parent) {
		super(parent, 0);
	}

	public void open() {		
		prefStore = MSP430Activator.getDefault().getPreferenceStore();
		
		Shell parent = getParent();
		dialogShell = new Shell(parent, SWT.DIALOG_TRIM|SWT.APPLICATION_MODAL|SWT.RESIZE);
		dialogShell.setText("Tool Manager");
		
		dialogShell.setLayout(new GridLayout(2,false));
		createControls(dialogShell);
		restoreList();
		
		dialogShell.layout();
		
		dialogShell.open();
		
		Display display = parent.getDisplay();
		while(!dialogShell.isDisposed()) {
			if( !display.readAndDispatch())
				display.sleep();
		}
	}
	
	private void createControls(Composite parent) {

		Composite listGroup = new Composite(parent, SWT.NONE);
		listGroup.setLayout(new GridLayout());
		GridData gd = new GridData();
        gd.verticalAlignment = GridData.FILL;
        gd.horizontalAlignment = GridData.FILL;
        gd.horizontalSpan = 1;
        gd.grabExcessHorizontalSpace = true;
        gd.grabExcessVerticalSpace = true;
        listGroup.setLayoutData(gd);
        
        createList(listGroup);
        
        Composite buttonGroup = new Composite(parent, SWT.NONE);
        buttonGroup.setLayout(new GridLayout(1,true));
		gd = new GridData();
        gd.verticalAlignment = GridData.FILL;
        gd.horizontalAlignment = GridData.END;
        gd.horizontalSpan = 1;
        gd.grabExcessVerticalSpace = true;
        buttonGroup.setLayoutData(gd);
        
        createButtons(buttonGroup);
	}
	
	private Composite createList(Composite parent) {
		toolsTable = new Table(parent, SWT.BORDER);
		toolsTable.setHeaderVisible(true);
		toolsTable.setLinesVisible(true);
		
		TableColumn nameColumn = new TableColumn(toolsTable, SWT.NONE);
		nameColumn.setText("Name");
		TableColumn toolColumn = new TableColumn(toolsTable, SWT.NONE);
		toolColumn.setText("Tools");		
		TableColumn pathColumn = new TableColumn(toolsTable, SWT.NONE);
		pathColumn.setText("Path");
		
		nameColumn.setWidth(200);
		toolColumn.setWidth(100);
		pathColumn.setWidth(50);
		
		GridData gd = new GridData();
        gd.verticalAlignment = GridData.FILL;
        gd.horizontalAlignment = GridData.FILL;
        gd.horizontalSpan = 1;
        gd.grabExcessHorizontalSpace = true;
        gd.grabExcessVerticalSpace = true;
        toolsTable.setLayoutData(gd);        
        
        return parent;
	}
	
	private Composite createButtons(Composite parent) {
		GridData gd;
		
		activateButton = new Button(parent, SWT.NONE);
		activateButton.setText("Activate");
		gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		activateButton.setLayoutData(gd);
		activateButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int item = toolsTable.getSelectionIndex();
				if( item > -1 ) {
					GenericToolProvider tp = (GenericToolProvider)toolsTable.getItem(item).getData();
	
					for (ToolType type : ToolType.values()) {
						if( tp.getTypes().contains(type)) {
							MSP430Activator.getDefault().getPreferenceStore().setValue(type.getProviderPreferenceName(), tp.getProviderId());
						}
					}
				}
			}			
		});
		
		addButton = new Button(parent, SWT.NONE);
		addButton.setText("Add...");
		gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		addButton.setLayoutData(gd);
		addButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dlg = new DirectoryDialog(getParent());
				String dir = dlg.open();
				if( dir != null) {
					GenericToolProvider tp = addDirectory(dir);
					MSP430Activator.getDefault().getToolchainManager().removeToolProvider(tp);
					MSP430Activator.getDefault().getToolchainManager().registerToolProvider(tp);
					saveList();
				}
			}
		});
		
		removeButton = new Button(parent, SWT.NONE);
		removeButton.setText("Remove");
		gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		removeButton.setLayoutData(gd);
		
		removeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int item = toolsTable.getSelectionIndex();
				if( item > -1 ) {
					GenericToolProvider tp = (GenericToolProvider)toolsTable.getItem(item).getData();
					MSP430Activator.getDefault().getToolchainManager().removeToolProvider(tp);
					
					toolsTable.remove(item);
					saveList();
				}
			}
		});
		
		closeButton = new Button(parent, SWT.NONE);
		closeButton.setText("Close");
		gd = new GridData();
		gd.grabExcessVerticalSpace = true;
		gd.verticalAlignment = SWT.END;
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		closeButton.setLayoutData(gd);
		
		closeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				dialogShell.close();
			}
		});
		
		return parent;
	}
	
	private void restoreList() {
		StringTokenizer tok = new StringTokenizer(prefStore.getString(PreferenceConstants.P_TOOL_DIRECTORIES), ";");
		while(tok.hasMoreTokens()) {
			addDirectory(tok.nextToken());
		}
	}
	
	private void saveList() {
		StringBuilder builder = new StringBuilder();
		for(int i=0; i<toolsTable.getItemCount(); i++) {
			builder.append(toolsTable.getItem(i).getText(2));
			
			if( i < toolsTable.getItemCount()-1) {
				builder.append(";");
			}
		}
		
		prefStore.setValue(PreferenceConstants.P_TOOL_DIRECTORIES, builder.toString());
	}
	
	private GenericToolProvider addDirectory(String path) {
		try {
			GenericToolProvider tp = new GenericToolProvider(path);
			TableItem t = new TableItem(toolsTable, SWT.NONE);
			t.setText(new String[]{tp.getName(), tp.getProvidedTools(), path});
			t.setData(tp);
			
			return tp;
		} catch(Exception e) {
			
		}
		return null;
	}
}
