package dk.xpg.msp430eclipse.toolinfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.IMSP430ToolProvider.ToolType;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class GCCTargets {
	private String toolCommand;
	private Pattern linkerFilePattern;

	public GCCTargets() {
	}

	synchronized public List<String> loadMCUList() throws IOException, ToolchainNotFoundException {
		toolCommand = MSP430Activator.getDefault().getToolchainManager().getCurrentProvider(ToolType.GCC).getExecutable(ToolType.GCC);

		List<String> mcuList = null;
		linkerFilePattern = Pattern.compile("(.*)\\.x.{0,2}$");
		
		String lineBegin = "libraries: =";

		ProcessBuilder pb = new ProcessBuilder(toolCommand,
				"-print-search-dirs");
		Map<String, String> env = pb.environment();
		env.put("LANG", "en_us");
		Process p = pb.start();
		BufferedReader lineReader = new BufferedReader(new InputStreamReader(
				p.getInputStream()));

		String[] libSearchPath = null;
		while (true) {
			String line = lineReader.readLine();
			if (line == null) {
				break;
			}
			if (line.startsWith(lineBegin)) {
				String s = line.substring(lineBegin.length());
				System.err.println("Search path: '" + s + "'");
				libSearchPath = s.split(":");
			}

		}

		if (libSearchPath != null) {
			for (String path : libSearchPath) {
				mcuList = extractMcuList(path);
				if (mcuList != null) {
					break;
				}
			}
		}
		
		return mcuList;
	}

	private List<String> extractMcuList(String searchPath) {
		File path = new File(searchPath);
		if (!path.exists() || !path.isDirectory()) {
			return null;
		}

		File ldscripts = null;
		File[] children = path.listFiles();
		for (File child : children) {
			if (child.getName().equals("ldscripts")) {
				ldscripts = child;
				break;
			}
		}

		if (ldscripts == null) {
			return null;
		}

		String[] mcus = ldscripts.list();
		Arrays.sort(mcus);
		ArrayList<String> mcuList = new ArrayList<String>();
		/*
		 * This loops through the files in the ldscript directory and deals with
		 * two situations: 1. Each file is actually a directory named after an
		 * MCU, meaning that we can use its name directory 2. There are multiple
		 * linker files all named something with .x. In that situation we remove
		 * that extension and hope it works.
		 * 
		 * This is not optimal, but I know of no other way to get to the
		 * supported MCUs
		 */
		for (String mcu : mcus) {
			Matcher m = linkerFilePattern.matcher(mcu);
			if (m.matches()) {
				if (!mcuList.contains(m.group(1))) {
					mcuList.add(m.group(1));
				}
			} else {
				mcuList.add(mcu);
			}
		}
		return mcuList;
	}
}
