package dk.xpg.msp430eclipse.toolinfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class Targets {
	private static Targets instance;
	private GCCTargets provider = new GCCTargets();

	public static Targets getInstance() {
		if (instance == null) {
			instance = new Targets();
		}
		return instance;
	}

	private List<String> mcuList;
	private Map<String,List<String>> categories;

	public Targets() {
		mcuList = null;
	}

	private Map<String,List<String>> initCategories() throws IOException,
			ToolchainNotFoundException {
		Map<String,List<String>> ret = new HashMap<String,List<String>>();
		List<String> mcuList = getMCUList();

		/* Sort the MCU list into categories by series.
		 * 
		 * The format of MSP430 MCUs is
		 * <Type>430<Type><Generation>
		 * 
		 * Family := CC | MSP | XMS
		 * 
		 * Type := C | F | FR | G | L | FG | CG | FE |
		 *         FW | AFE | BT | BQ
		 *         
		 * Generation := [0-6]
		 * 
		 */
		final String[] families = new String[] {
			"cc430", "msp430", "xms430"
		};
		
		final String[] types = new String[] {
			"fg", "cg", "fe", "fr",
			"fw", "afe", "bt", "bq",
			"c", "f", "g", "l"
		};
		
		final String[] groupTypes = new String[] {
			"fr", "f", "g"	
		};
		
		final String othersGroupName = "Others";
		
		for (String mcu : mcuList) {
			try {
				String s = null;
				String family = null;
				String type = null;
				String generation = null;
				boolean ok = false;
				for(String f: families) {
					if( mcu.startsWith(f)) {
						s = mcu.substring(f.length());
						family = f;
						ok = true;
						break;
					}
				}
				if( !ok ) {
					throw new UnknownGroupException();
				}
				ok = false;
				for(String t: types ) {
					if( s.startsWith(t)) {
						s = s.substring(t.length());
						type = t;
						ok = true;
						break;
					}
				}
				if( !ok ) {
					throw new UnknownGroupException();
				}
				generation = s.substring(0, 1);
				ok = false;
				for(String t: groupTypes ) {
					if( type.equals(t) ) {
						ok = true;
						break;
					}
				}
				if( !ok ) {
					throw new UnknownGroupException();
				}
				
				String category = 
						family.toUpperCase() + " " + type.toUpperCase() + generation + "-series";
				if( !ret.containsKey(category) ) {
					ret.put(category, new ArrayList<String>());
				}
				ret.get(category).add(mcu);
			} catch (StringIndexOutOfBoundsException e) {

			} catch (UnknownGroupException e) {
				/* No, it isn't pretty nor fast to use exceptions
				 * for regular control flow, but it does the trick
				 * for me right now. Will refactor this code later
				 */
				if( !ret.containsKey(othersGroupName) ) {
					ret.put(othersGroupName, new ArrayList<String>());
				}
				ret.get(othersGroupName).add(mcu);				
			}
		}

		return ret;
	}

	public Set<String> getCategories() throws IOException,
			ToolchainNotFoundException {
		if (categories == null) {
			categories = initCategories();
		}
		return categories.keySet();
	}

	public List<String> getMCUList(String category) {
		return categories.get(category);
	}
	
	public String getCategory(String mcu) throws IOException, ToolchainNotFoundException {
		/* This is not very efficient, but who cares at
		 * this point :-)
		 */
		if( categories == null) {
			categories = initCategories();
		}
		
		for(String c: categories.keySet()) {
			if( categories.get(c).contains(mcu.toLowerCase())) {
				return c;
			}
		}
		return null;
	}

	public List<String> getMCUList() throws IOException,
			ToolchainNotFoundException {
		if (mcuList == null) {
			mcuList = provider.loadMCUList();
		}
		return mcuList;
	}

	public void clearList() {
		mcuList = null;
	}
	
	private class UnknownGroupException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
	}
}
