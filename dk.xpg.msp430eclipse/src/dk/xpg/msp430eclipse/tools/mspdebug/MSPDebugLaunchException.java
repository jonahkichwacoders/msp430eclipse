package dk.xpg.msp430eclipse.tools.mspdebug;

public class MSPDebugLaunchException extends Exception {
	private static final long serialVersionUID = 6301000287151401442L;

	public MSPDebugLaunchException(String message) {
		super(message);
	}
	
	public MSPDebugLaunchException(String message, Throwable cause) {
		super(message, cause);
	}
}
