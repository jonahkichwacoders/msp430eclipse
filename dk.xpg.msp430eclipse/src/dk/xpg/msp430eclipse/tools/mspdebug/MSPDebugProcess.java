package dk.xpg.msp430eclipse.tools.mspdebug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.console.IOConsoleOutputStream;
import org.eclipse.ui.console.MessageConsole;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.toolchain.ToolchainNotFoundException;

public class MSPDebugProcess {
	private MSPDebug mspDebug;
	private volatile Process process;
	private List<String> errorOut;

	private boolean backgroundNotify;
	private IStatus status;
	private String expectedLine;
	private Object sync;

	private static final String NEWLINE = System.getProperty("line.separator");

	public MSPDebugProcess(MSPDebug mspDebug) {
		this.mspDebug = mspDebug;
		backgroundNotify = false;
		status = Status.OK_STATUS;
		sync = new Object();
	}

	public void runBackground(String expectedLine) throws CoreException {
		this.expectedLine = expectedLine;
		final MSPDebugProcess parent = this;
		Thread workerThread = new Thread() {
			public void run() {
				status = parent.run(new NullProgressMonitor());
				backgroundNotify = true;
				synchronized (sync) {
					sync.notifyAll();
				}
			}
		};

		workerThread.start();
		try {
			long start = System.currentTimeMillis();
			synchronized (sync) {
				while (backgroundNotify == false) {					
					sync.wait(10000);
					if( System.currentTimeMillis() > start + 10000) {
						process.destroy();
						throw new CoreException(new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
								"Giving up waiting for mspdebug"));
					}
				}
			}
		} catch (InterruptedException e) {

		}
		if (status != Status.OK_STATUS) {
			throw new CoreException(status);
		}
	}

	public IStatus run(final IProgressMonitor monitor) {
		IStatus status = Status.OK_STATUS;
		MessageConsole console = MSP430Activator.getDefault().getConsole(
				"MSPDebug");
		console.clearConsole();
		console.activate();
		final IOConsoleOutputStream consoleStream = console.newOutputStream();

		try {
			errorOut = new LinkedList<String>();
			monitor.beginTask("Launching MSPDebug", IProgressMonitor.UNKNOWN);
			process = mspDebug.launch();
			
			Thread errorReaderThread = new ReaderThread(process.getErrorStream(),
					new LineHandler() {
						@Override
						public void handle(String line) {
							errorOut.add(line);
							try {
								consoleStream.write(line);
								consoleStream.write(NEWLINE);
							} catch (IOException e) {
							}
						}
					});
			
			Thread outputReaderThread = new ReaderThread(process.getInputStream(),
					new LineHandler() {

						@Override
						public void handle(String line) {
							if (expectedLine != null
									&& line.equals(expectedLine)) {
								synchronized (sync) {
									backgroundNotify = true;
									sync.notifyAll();
								}
							}
							try {
								consoleStream.write(line);
								consoleStream.write(NEWLINE);
							} catch (IOException e) {
								System.err.println("Failed to write to console");
							}

						}

					});
			
			outputReaderThread.start();
			errorReaderThread.start();
			
			while (true) {
				try {
					consoleStream.write("Waiting for mspdebug to finish\n");
					process.waitFor();
					break;
				} catch (InterruptedException e) {
					e.printStackTrace();
					// Ignore
				}
			}

			try {
				errorReaderThread.join();
				outputReaderThread.join();
			} catch (InterruptedException e) {
			}

			if (process.exitValue() != 0) {
				MSPDebugErrorParser errorParser = new MSPDebugErrorParser(
						errorOut);
				status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
						errorParser.getErrorMessage(), errorParser.getError());						
			}
		} catch (MSPDebugLaunchException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
					"Failed to launch MSPDebug: " + e.getMessage());
		} catch (IOException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
					"I/O Error", e);
		} catch (ToolchainNotFoundException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
					"Could not locate mspdebug", e);
		} catch (URISyntaxException e) {
			status = new Status(Status.ERROR, MSP430Activator.PLUGIN_ID,
					"URISyntaxException", e);
		} finally {
			try {
				consoleStream.close();
			} catch (IOException e) {
			}
			monitor.done();
		}
		synchronized (sync) {
			backgroundNotify = true;
			sync.notifyAll();
		}

		return status;
	}

	private interface LineHandler {
		public void handle(String line);

	}

	private class ReaderThread extends Thread {
		private BufferedReader reader;
		private LineHandler lineHandler;

		public ReaderThread(InputStream inputStream, LineHandler lineHandler) {
			this.reader = new BufferedReader(
					new InputStreamReader(inputStream));
			this.lineHandler = lineHandler;
		}

		@Override
		public void run() {
			try {
				while (true) {
					String line = reader.readLine();
					if (line == null) {
						break;
					}

					lineHandler.handle(line);
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		if ( process != null ) {
			process.destroy();
		}
	}
}
