package dk.xpg.msp430eclipse.ui.wizards;

import org.eclipse.cdt.managedbuilder.ui.wizards.MBSCustomPage;
import org.eclipse.cdt.managedbuilder.ui.wizards.MBSCustomPageData;
import org.eclipse.cdt.managedbuilder.ui.wizards.MBSCustomPageManager;
import org.eclipse.cdt.ui.wizards.CDTCommonProjectWizard;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import dk.xpg.msp430eclipse.MSP430Activator;
import dk.xpg.msp430eclipse.MSP430PropertyManager;
import dk.xpg.msp430eclipse.managedbuild.ConfigurationMacroNames;
import dk.xpg.msp430eclipse.natures.MSP430ProjectNature;
import dk.xpg.msp430eclipse.ui.MCUSelection;

public class TargetSelectPage extends MBSCustomPage implements Runnable {

//	private Composite top;
//	private Combo mcuCombo;
	private String selectedMCU;
	private MCUSelection mcuSelection;
	
	public TargetSelectPage() {
		super("dk.xpg.msp430eclipse.targetselectpage");
		selectedMCU = "";
	}

	@Override
	public String getName() {
		return "MSP430 Target Selection";
	}

	@Override
	public void createControl(Composite parent) {
		mcuSelection = new MCUSelection(parent);
		mcuSelection.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				selectedMCU = (String)arg0.text;
			}
			
		});
	}

	@Override
	public void dispose() { 
		mcuSelection.getTop().dispose();
	}

	@Override
	public Control getControl() {
		if( mcuSelection == null) 
			return null;
		return mcuSelection.getTop();
	}

	@Override
	public String getDescription() {
		return "";
	}

	@Override
	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Image getImage() {
		return wizard.getDefaultPageImage();
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTitle() {
		return "MSP430 Target Hardware Selection";
	}

	@Override
	public void performHelp() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDescription(String description) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setImageDescriptor(ImageDescriptor image) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setTitle(String title) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setVisible(boolean visible) {
		//top.setVisible(visible);
		mcuSelection.getTop().setVisible(visible);
	}

	@Override
	public void run() {
		/* Borrowed from AVR Eclipse plugin */
		MBSCustomPageData pagedata = MBSCustomPageManager.getPageData(pageID);
		CDTCommonProjectWizard wizz = (CDTCommonProjectWizard) pagedata
				.getWizardPage().getWizard();
		IProject project = wizz.getLastProject();

		MSP430PropertyManager props = MSP430PropertyManager
				.getInstance(project);				
		props.setPropertyValue(ConfigurationMacroNames.MSP430TARGETMCU.name(),
				((TargetSelectPage)pagedata.getWizardPage()).selectedMCU);
		
		try {
			MSP430ProjectNature.addMSP430Nature(project);
		} catch (CoreException ce) {
			IStatus status = new Status(IStatus.ERROR, MSP430Activator.PLUGIN_ID,
					"Could not add MSP430 nature to project ["
							+ project.toString() + "]", ce);
			//Activator.getDefault().log(status);
		}
	}

	@Override
	protected boolean isCustomPageComplete() {
		return true;
	}

}
